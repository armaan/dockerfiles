# kiss-docker
A dockerized version of [KISS Linux](https://k1ss.org/), good for use either as a base image or an interactive operating system

Upon running with it `-it` flags, you should be dropped into an `ash` shell in KISS! Make sure to mount some volumes to persist data. Repositories are stored in `/usr/repos`, and both the community and official repos are installed by default. Total image size is about 200MB
