This is my personal build of php-apache with some nice extra things that the official images do not provide, such as Redis support.

Requires a working wordpress installation to be mounted at /var/ww/html inside the container if being used for that (what it is intended for)
