# mdBook

WARNING: This is probably not the most recent version of mdBook, as I do not maintain this image very actively.

Runs [mdBook](github.com/rust-lang/mdBook/) in a docker container. To use, you need to mount an initialized book to `/book` in the docker container. You can initialize a book using `docker run -v PATH/TO/BOOK:/book armaanb/mdbook mdbook init`. Then serve it using `docker run -v PATH/TO/BOOK:/book -p 3000:3000 armaanb/mdbook`
